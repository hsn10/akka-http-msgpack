name := "MsgPack for Akka-HTTP"

normalizedName := "akka-http-msgpack"

version := "0.0.5"

organization := "com.filez.scala.akka"

organizationName := "Filez.com"

startYear := Some(2020)

homepage := Some(url("https://gitlab.com/hsn10/akka-http-msgpack"))

licenses += "MIT" -> url("https://opensource.org/licenses/MIT")

developers += Developer("hsn10","Radim Kolar","hsn@sendmail.cz",url("https://netmag.ml"))

scmInfo := Some(ScmInfo(url("https://gitlab.com/hsn10/akka-http-msgpack"),"scm:git:https://gitlab.com/hsn10/akka-http-msgpack.git"))

crossScalaVersions := Seq("2.12.11", "2.13.3")

ThisBuild / scalaVersion := crossScalaVersions.value.head

scalacOptions ++= Seq(
  "-feature",
  "-deprecation"
)

libraryDependencies ++= Seq(
			"org.scalatest" %% "scalatest" % "3.2.0" % Test,
			"com.typesafe.akka" %% "akka-actor-testkit-typed" % "2.6.6" % Test
)

libraryDependencies += "org.wvlet.airframe" %% "airframe-codec" % "20.6.1"

libraryDependencies ++= Seq(
"com.typesafe.akka" %% "akka-http"   % "10.1.12",
"com.typesafe.akka" %% "akka-stream" % "2.6.6"
)

fork := true

pomIncludeRepository := { _ => false }

publishMavenStyle := true

publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases"  at nexus + "service/local/staging/deploy/maven2")
}
