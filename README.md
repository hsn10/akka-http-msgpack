## msgPack Marshalling support for Akka-HTTP

[![pipeline status](https://gitlab.com/hsn10/akka-http-msgpack/badges/master/pipeline.svg)](https://gitlab.com/hsn10/akka-http-msgpack/-/commits/master)
[![](https://tokei.rs/b1/gitlab/hsn10/akka-http-msgpack?category=code)](https://github.com/XAMPPRocky/tokei)

Object serialization to [msgpack](https://msgpack.org) format. Content type is _application/x-msgpack_. It is similar to [JSON](https://en.wikipedia.org/wiki/JSON) but binary and smaller then [BSON](https://en.wikipedia.org/wiki/BSON).

#### How to use:

```
import com.filez.scala.akka.msgpack._
implicit val marchaller = MsgPackMarshaller.toMsgPackEntity[Type to marchall]
implicit val unmarch = MsgPackUnmarshaller.fromMsgPackEntity[Type]
```

#### SBT

```
libraryDependencies += "com.filez.scala.akka" %% "akka-http-msgpack" % "0.0.5"
```

#### Maven Central publish

```
sbt +publishSigned
```