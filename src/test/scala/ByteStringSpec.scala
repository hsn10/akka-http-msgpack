package com.filez.scala.akka.msgpack

import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.unmarshalling._
import akka.util.ByteString
import akka.actor.testkit.typed.scaladsl._

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures._
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

import java.time.Instant
import akka.stream.Materializer

case class P(time: Instant, instr: String, price: Double)

class ByteStringSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike with Matchers {
    implicit val ec  = scala.concurrent.ExecutionContext.global
    implicit val mat = Materializer.matFromSystem(testKit.internalSystem.classicSystem)

    "ByteString Marshaller" must {
        "marshall to bytestring without an exception" in {
            val p = P(Instant.now(),"EURO",1.16)
            val march = MsgPackMarshaller.toMsgPack[P]
            val fut = Marshal(p).to[ByteString](march,ec)
            whenReady ( fut ) { l =>
                l should not be (null)
                l.length should be > 10
            }
        }
    }
    "ByteString Unmarshaller" must {
        "construct original object" in {
            val o = P(Instant.now(),"EURO",1.16)
            val march = MsgPackMarshaller.toMsgPack[P]
            val fut1 = Marshal(o).to[ByteString](march,ec)
			whenReady ( fut1 ) { bytes =>
			   val unmarch = MsgPackUnmarshaller.fromMsgPack[P]
               val fut2 = Unmarshal(bytes).to[P](unmarch, ec, mat)
               whenReady ( fut2 ) { p =>
                  p should equal(o)
               }
			}
        }
    }
}