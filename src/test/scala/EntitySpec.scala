package com.filez.scala.akka.msgpack

import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.unmarshalling._
import akka.http.scaladsl.model._
import akka.util.ByteString
import akka.actor.testkit.typed.scaladsl._

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures._
import org.scalatest.wordspec.AnyWordSpecLike

import java.time.Instant
import akka.stream.Materializer

class EntitySpec extends ScalaTestWithActorTestKit with AnyWordSpecLike {
    implicit val ec  = scala.concurrent.ExecutionContext.global
    implicit val mat = Materializer.matFromSystem(testKit.internalSystem.classicSystem)

    "Entity Marshaller" must {
        "marshall to MessageEntity without an exception" in {
            val p = P(Instant.now(),"EURO",1.16)
            val march = MsgPackMarshaller.toMsgPackEntity[P]
            val fut = Marshal(p).to[HttpEntity](march,ec)
            whenReady ( fut ) { l =>
                l should not be (null)
            }
        }
    }
    "Entity Unmarshaller" must {
        "reconstruct original object" in {
            val o = P(Instant.now(),"EURO",1.16)
            val march = MsgPackMarshaller.toMsgPackEntity[P]
            val fut1 = Marshal(o).to[HttpEntity](march,ec)
			whenReady ( fut1 ) { ent =>
			   val unmarch = MsgPackUnmarshaller.fromMsgPackEntity[P]
               val fut2 = Unmarshal(ent).to[P](unmarch, ec, mat)
               whenReady ( fut2 ) { p =>
                  p should equal(o)
               }
			}
        }
    }
}