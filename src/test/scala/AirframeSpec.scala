package com.filez.scala.akka.msgpack

import org.scalatest._
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers

import wvlet.airframe.msgpack.spi.MessagePack
import wvlet.airframe.codec.MessageCodec

class AirframeSpec extends AnyWordSpec with Matchers {
    "Airframe msgpack packer" must {
        "be able to pack and back using pack* methods" in {
           val packer = MessagePack.newBufferPacker
           packer.packInt(10)
           packer.packString("hello")
           // Produce MessagePack byte array
           val msgpack = packer.toByteArray

           // Create an unpacker for reading MesagePack values 
           val unpacker = MessagePack.newUnpacker(msgpack)
           unpacker.unpackInt must equal (10) 
           unpacker.unpackString must equal ("hello")
        }
    }
    "Airframe msgpack codec" must {
        "be able to store/restore object" in {
           val codec = MessageCodec.of[P]
		   val a = P(java.time.Instant.now(), "leo", 1.34)
		   val msgpack : Array[Byte] = codec.toMsgPack(a)
           val a2 = codec.fromMsgPack(msgpack)
           a2 must equal (a)
        }
    }
}
