package com.filez.scala.akka.msgpack

import scala.language.implicitConversions
import scala.reflect.runtime.universe._

import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.model._
import akka.util.ByteString
import wvlet.airframe.codec.MessageCodec

object MsgPackMarshaller {
   val contentType = ContentType(MediaType.customBinary("application","x-msgpack",new MediaType.Compressibility(false)))
   implicit def toMsgPack[T:TypeTag] : ToByteStringMarshaller[T] = {
      Marshaller.opaque[T,ByteString]{ in =>
         val codec = MessageCodec.of[T]
         val packed = codec.toMsgPack(in)
         ByteString(packed)  
      }
   }
   implicit def toMsgPackEntity[T:TypeTag]: ToEntityMarshaller[T] = {
      Marshaller.withFixedContentType[T,MessageEntity](contentType){ in =>
            val codec = MessageCodec.of[T]
            HttpEntity(contentType,codec.toMsgPack(in))
      }
   }
}