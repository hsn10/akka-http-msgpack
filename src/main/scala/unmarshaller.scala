package com.filez.scala.akka.msgpack

import scala.language.implicitConversions
import scala.reflect.runtime.universe._

import akka.http.scaladsl.unmarshalling._
import akka.http.scaladsl.model._
import akka.http.scaladsl.util.FastFuture
import akka.util.ByteString
import akka.stream.scaladsl._
import wvlet.airframe.codec.MessageCodec

object MsgPackUnmarshaller {
   /** we unmarshall also this content-type */
   val alternateContentType = ContentType(MediaType.customBinary("application","msgpack",new MediaType.Compressibility(false)))
   implicit def fromMsgPack[T:TypeTag] : FromByteStringUnmarshaller[T] = {
      Unmarshaller.strict[ByteString,T]{ in =>
         val codec = MessageCodec.of[T]
         codec.fromMsgPack(in.toArray)
      }
   }
   implicit def fromMsgPackEntity[T:TypeTag] : FromEntityUnmarshaller[T] = {
      Unmarshaller.withMaterializer[HttpEntity,T]{ implicit ec => implicit mat => in =>
         if (in.contentType != MsgPackMarshaller.contentType &&
             in.contentType != MsgPackUnmarshaller.alternateContentType) {
             FastFuture.failed(new IllegalArgumentException("Unsupported content type")) 
         } else
         {
            in.dataBytes.toMat(Sink.fold(ByteString.empty)( (a,i) => a.concat(i) ))( (_,fa) => fa.map( _.toArray))
            .run()
            .map( ar => MessageCodec.of[T].fromMsgPack(ar))
         }
      }
   }
}